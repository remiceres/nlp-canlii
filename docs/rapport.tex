\documentclass{article}

% Langue
\usepackage{polyglossia}
\setdefaultlanguage{french}
\usepackage[autostyle=true]{csquotes}
\usepackage{microtype}
\usepackage[
    output-decimal-marker={,},
    group-separator={\,},
]{siunitx}

% Police
\usepackage{fontspec}
\usepackage{xunicode}

\setmainfont[Mapping=tex-text]{Linux Libertine O}
\setsansfont[Mapping=tex-text]{XCharter}

\usepackage[raggedright, sf]{titlesec}
\titleformat{\paragraph}[runin]{\bfseries\normalsize}{\theparagraph}{1em}{}

% Mise en page
\usepackage{geometry}
\usepackage{lipsum}

% Listes
\usepackage{enumitem}

% Symboles
\usepackage{pifont}
\newcommand{\cmark}{\ding{51}}
\newcommand{\xmark}{\ding{55}}

% Figures
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{rotating}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usetikzlibrary{arrows, arrows.meta}
\usetikzlibrary{chains}
\usetikzlibrary{fit}
\usepackage{pgfplots}
\pgfplotsset{compat=1.16}

\usepackage{framed}

% Tableaux
\usepackage{booktabs}
\usepackage{multirow}

% Extraits de code
\usepackage{listings}
\usepackage{xcolor}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    frame=ltb,
    framexleftmargin=8pt,
    xleftmargin=8pt,
    framextopmargin=8pt,
    framexbottommargin=8pt,
    framerule=0pt,
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2,
    escapeinside={(*}{*)},
}

\lstset{style=mystyle}

% Hyperliens
\usepackage{hyperref}

% Métadonnées
\title{
    Extraction d’informations des décisions de la~RACJ\\[.25em]
    \large IFT-7022 --- Traitement automatique de la langue naturelle\\
    Travail pratique n°3
}
\author{Rémi Cérès \and Mattéo Delabre}
\date{6 mai 2020}

\begin{document}

% Désactive l’espacement automatique avant certains signes de ponctuation
\makeatletter
\nofrench@punctuation
\renewcommand{\enquote}[1]{«~#1~»}
\makeatother

\maketitle

% https://www.racj.gouv.qc.ca/a-propos-de-la-regie/mandat.html
% http://legisquebec.gouv.qc.ca/fr/ShowDoc/cs/R-6.1

La régie des alcools, des courses et des jeux du Québec~(RACJ) est un organisme officiel, institué en~1993 par la loi~R-6.1, qui a pour vocation d’encadrer les activités ayant trait à la vente de boissons alcooliques, aux jeux, aux sports de combat professionnels et aux courses de chevaux.
Son mandat inclut notamment la gestion de permis et de licences autorisant la pratique de ces activités, la surveillance et le contrôle de leur déroulement et l’imposition de sanctions lorsque cela est nécessaire.
L’une des problématiques majeures rencontrées par la régie lorsqu’elle doit statuer sur un cas est le respect de la cohérence par rapport aux décisions prises antérieurement, puisque cela demande aux régisseurs de rechercher manuellement les cas pertinents et d’en extraire les informations d’intérêt.
Cet exercice pourrait être rendu plus efficace si les informations des décisions existantes étaient archivées dans un format structuré, permettant de rechercher des cas et de les synthétiser selon certains critères.

L’objectif de ce troisième travail pratique est d’automatiser l’extraction d’informations à partir des décisions de la~RACJ en utilisant des techniques de traitement automatique de la langue naturelle.
Nous avons réalisé ce projet avec le concours de M\textsuperscript{e}~Marc~Lajoie, juriste à la~RACJ, qui nous a notamment informé sur le type de métadonnées qui lui seraient utiles dans son travail.

\tableofcontents

\section{Analyse des besoins}

\subsection{Étude des informations à extraire des documents}

La régie est amenée à rendre des décisions qui sont matérialisées par la publication de documents similaires à celui présenté en figure~\ref{fig:anatomie-decision}.
Dans chacun de ces documents est exposé le contexte de l’affaire concernée, le droit en rapport puis, après analyse des différents éléments, le verdict prononcé.
Le projet sur lequel nous avons travaillé a pour objectif d’extraire des informations permettant de faciliter la recherche dans les documents des décisions existantes en fonction de caractéristiques couramment utilisées par les employés de la régie dans leur travail.

\begin{figure}[htb]
    \centering
    \begin{tikzpicture}
        \tikzset{
            label/.style={
                anchor=west,
                font=\itshape,
            },
            label link/.style={
                -Circle[],
            }
        }
        \node[draw, anchor=south west, rounded corners=1, opacity=0.7]
            {\includegraphics[width=.5\textwidth]{fig/decision}};
            
        \begin{pgfinterruptboundingbox}
            \node[label] (hdr) at (8.5, 6.35) {En-tête};
            \node[label] (fda) at (8.5, 4) {Faits, droit et analyse};
            \node[label] (ver) at (8.5, 1.5) {Verdict};
            
            \coordinate (align) at ($(hdr)+(-3, 0)$);
            
            \draw[label link] (hdr) -- (hdr-|align);
            \draw[label link] (fda) -- (fda-|align);
            \draw[label link] (ver) -- (ver-|align);
        \end{pgfinterruptboundingbox}
    \end{tikzpicture}
    \caption{Anatomie d’une décision rendue par la RACJ.}
    \label{fig:anatomie-decision}
\end{figure}

Un premier groupe d’informations utiles sont celles qui permettent d’\textbf{identifier} la décision en question, à savoir son numéro unique, la date où elle a été rendue, le numéro de dossier dont elle relève ainsi que la date et le lieu de l’audience associée.
L’extraction de ces informations peut par exemple permettre de rechercher une décision rendue pendant une certaine période ou de rechercher toutes les décisions relevant d’un même dossier.
Les informations d’identification sont regroupées dans l’en-tête de chaque décision.

Ces décisions peuvent être de différentes \textbf{natures}, la plus courante étant celle des décisions résultant de la constatation de manquements relatifs à des permis gérés par la régie, amenant potentiellement à des sanctions telles que la suspension des permis concernés ou l’imposition d’amendes.
Elles peuvent également répondre à des demandes d’octroi ou de modification de permis, ou arbitrer d’une opposition d’un tiers à un tel octroi.
L’extraction de cette information, également située dans l’en-tête du document, permet de cibler les décisions d’un même type qui seraient pertinentes par rapport à une tâche donnée.

Lorsque la décision concerne l’exploitation de permis, les informations sur \textbf{l’établissement concerné} (sa raison sociale, son adresse et le nom de son responsable) ainsi que sur \textbf{les permis concernés} (leur numéro d’identification et leur type) sont également utiles pour cibler les décisions relevant d’un même type de permis ou d’un même établissement.
Ces deux catégories d’informations peuvent être situées dans l’en-tête du document ou en pied de page.

Enfin, dans le dernier segment du document est précisé le \textbf{verdict} prononcé.
La récupération de ce verdict sous forme structurée est importante pour pouvoir extraire des statistiques sur les types de sanctions attribuées selon les cas, et ainsi faciliter la prise de décisions cohérentes avec la jurisprudence.
La régie peut par exemple décider de ne pas intervenir sur le cas concerné ou, à l’inverse, d’imposer la suspension voire la révocation d’un permis, ou encore d’imposer une amende (également appelée \emph{sanction administrative pécuniaire}) au détenteur du permis.

\subsection{Méthodologie pour l’extraction des informations}

Toutes les décisions rendues par la~RACJ sont mises à disposition du public sur le portail CanLII~(site web de l’institut canadien d'information juridique), au format~Word exporté en~HTML\footnote{Voir~: \url{https://www.canlii.org/fr/qc/qcracj/}}.
Dans la mesure où CanLII ne permet pas l’export massif de décisions et où nous n’avons pas pu obtenir les fichiers en question directement depuis la régie, nous avons décidé de les extraire du site en nous appuyant sur la bibliothèque Scrapy\footnote{Voir~: \url{https://scrapy.org/}}.
Le corpus que nous avons constitué pour analyse comprend ainsi près de~16\,000 décisions rendues par la régie entre~2000 et~2019 qui peuvent être récupérées depuis le lien suivant~:\\ \url{https://next.cloud.delab.re/s/D73dX2bcCWPZMPz} (110~Mo compressés, 722~Mo réels).

Les décisions de ce corpus suivent un format régulier et sont rédigées dans un style qui leur est propre, assez éloigné du français tel qu’employé dans les nouvelles ou la littérature.
Ces spécificités font, d’une part, qu’il est faisable d’adopter une approche à base de règles pour l’extraction d’informations, puisque les données recherchées suivent généralement un ensemble restreint de motifs identifiables et, d’autre part, que les modèles de langues tirés de corpus plus classiques sont inadaptés à la tâche.
L’approche à base de règles est d’autant plus adaptée au problème traité que nous n’avons pas à notre disposition d’exemples de données déjà étiquetées qui seraient nécessaires à une approche statistique.

Pour implémenter cette méthode, nous avons choisi d’utiliser la bibliothèque de traitement de la langue naturelle \emph{spaCy}, qui propose un module permettant de faciliter l’écriture de ce type de règles en s’appuyant sur une extension des expressions rationnelles qui fonctionne sur des séquences de jetons plutôt que sur des chaînes de caractères classiques\footnote{Voir~: \url{https://spacy.io/usage/rule-based-matching}}.
En outre, nous avons été contraints d’utiliser le modèle de langue fourni par défaut avec \emph{spaCy} faute d’en avoir trouvé un plus adapté~; ce modèle étant construit sur un corpus de nouvelles, nous avons dû y réaliser plusieurs modifications pour l’adapter à notre cas d’utilisation.

\section{Processus d’extraction d’informations}

Notre programme d’extraction d’informations est organisé sous forme d’un processus de traitement~(ou \emph{pipeline}) contenant six phases principales représentées en figure~\ref{fig:pipeline}.
Les cinq premières étapes rassemblent des pré-traitements nécessaires pour convertir les décisions, initialement au format~HTML, en un format dont il est plus aisé d’extraire les informations recherchées.
L’ultime étape du processus tire parti de la nouvelle forme du texte pour y identifier les informations ciblées.

\begin{figure}[htb]
\centering
\begin{tikzpicture}[
    start chain,
    init/.style={
        inner sep=0pt,
        outer sep=0pt,
    },
    phase/.style={
        draw,
        rounded corners=1,
        font=\strut,
        inner xsep=6pt,
        inner ysep=3pt,
    },
    every join/.style={
        -Latex,
    },
    node distance=0.5cm,
]
    \node[on chain, init] {};
    \node[on chain, phase, join] {Décodage};
    \node[on chain, phase, join] {\emph{Tokenization}};
    \node[on chain, phase, join] {Fusion};
    \node[on chain, phase, join] {Segmentation};
    \node[on chain, phase, join] {N.E.R.};
    \node[on chain, phase, join] {Extraction};
    
    \coordinate (center)
        at (barycentric cs:chain-2=0.5,chain-3=0.5,chain-4=0.5,chain-5=0.5,chain-6=0.5);
    \node[
        below=.5cm of center,
        font=\itshape
    ] (label) {Pré-traitements};
    
    \node[
        fit=(chain-2)(chain-3)(chain-4)(chain-5)(chain-6)(label),
        draw, dashed,
        rounded corners=2,
        inner xsep=8pt,
        inner ysep=6pt,
    ] {};
\end{tikzpicture}
\caption{Vue d’ensemble du processus de traitement des décisions pour l’extraction d’information.}
\label{fig:pipeline}
\end{figure}

\subsection{Pré-traitements sur le corpus}

Ces pré-traitements visent à filtrer les décisions originales pour ne conserver que les informations utiles à la tâche d’extraction.

\paragraph{Décodage du HTML}
Les décisions sont initialement en format HTML résultant d’un export de document~Word, dont le balisage contient principalement des informations de mise en forme non pertinentes pour l’extraction d’informations.
Nous effectuons un filtrage de chaque document avec la bibliothèque \texttt{HTMLParser} pour ne conserver que son texte et les balises sémantiques utiles qui y sont apposées par la régie pour identifier certaines informations importantes.
Ces balises, encadrant par exemple les noms des régisseurs ou les dates d’audiences, nous offrent en effet un point de départ intéressant pour l’extraction d’informations.
Une attention particulière est également prêtée à la gestion des caractères d’espacements, puisque dans le format HTML les successions de blancs sont par exemple censées être interprétées comme une seule espace, et les paragraphes sont ouverts par une balise spécifique.
En sortie de cette étape, le texte est dans une forme brute, où les paragraphes sont encodés comme des double sauts de ligne et les successions d’espaces sont compressées, afin de faciliter le travail du \emph{tokenizer} par la suite.

\paragraph{\emph{Tokenization}}
Pour séparer le texte en jetons, grâce à la préparation du texte réalisée à l’étape précédente, le moteur fourni par défaut avec~\emph{spaCy} est suffisant.
L’algorithme utilisé par la bibliothèque consiste d’abord à séparer les mots sur les caractères d’espacement, puis, pour chaque série de lettres ainsi rencontrée, à chercher à en séparer des préfixes ou des suffixes, voire à les couper sur des infixes spécifiques à la langue concernée.
La figure~\ref{fig:tokenization} montre un exemple de \emph{tokenization} par \emph{spaCy} d’une phrase en français.

\begin{figure}[htb]
\begin{tabular}{rl}
Forme initiale & \texttt{"Référez-vous à la section §3!"}\\
Découpe sur les blancs & \texttt{"Référez-vous", "à", "la", "section", "§3!"}\\
Séparation des préfixes & \texttt{"Référez-vous", "à", "la", "section", "§", "3!"}\\
Séparation des suffixes & \texttt{"Référez-vous", "à", "la", "section", "§", "3", "!"}\\
Découpe sur les infixes & \texttt{"Référez", "-", "vous", "à", "la", "section", "§", "3", "!"}\\
\end{tabular}
\caption{Exemple de fonctionnement de l’algorithme de \emph{tokenization} de \emph{spaCy}.}
\label{fig:tokenization}
\end{figure}

Un cas manquait cependant à l’appel dans le modèle français par défaut, celui demandant d’extraire l’unité monétaire d’expressions telles que \texttt{"Le prix est de 50\$."}.
Pour le traiter, il suffit cependant d’ajouter le caractère \texttt{\$} dans la liste des infixes du modèle.

\paragraph{Fusion de jetons consécutifs}
Ces règles simples de fonctionnement du \emph{tokenizer} montrent cependant leurs limites dans plusieurs situations pourtant utiles au domaine étudié.
Par exemple, lorsque l’algorithme rencontre des grands nombres dont les chiffres ont été groupés en tranches de trois et séparées par des espaces~(par exemple, certaines amendes imposées par la régie), son fonctionnement fait que le nombre sera transformé en autant de jetons distincts.
Pour faciliter l’extraction d’informations, il est utile de considérer les nombres comme constituant un seul jeton~: une étape de fusion est donc ajoutée après la \emph{tokenization} pour rassembler les séquences de nombres séparées par des espaces.
Les dates présentées au format standard \enquote{AAAA-MM-JJ} subissent également cette séparation inexacte, une règle similaire permet de les rassembler en un seul jeton, et permet également à cette occasion de leur attribuer une étiquette de date.
Enfin, certaines références alphanumériques (numéros de dossier ou de décisions, par exemple), peuvent parfois contenir également des tirets et sont fusionnés en suivant le même schéma, obtenant également une étiquette de référence utilisée pour identifier les informations contenant des références par la suite.

\paragraph{Segmentation}
\emph{spaCy} propose deux modes de segmentation des phrases~: l’un basé sur une approche statistique, et l’autre basé uniquement sur la ponctuation.
En raison des importantes différences entre le corpus utilisé pour entraîner le modèle statistique et le corpus des décisions de la~RACJ, le modèle statistique offre de moins bons résultats que la segmentation basée sur la ponctuation, que nous avons choisi d’utiliser.

\paragraph{Reconnaissance d’entités nommées}
Le modèle de langue français par défaut ne permet de reconnaître qu’un nombre limité d’entités nommées~: les personnes, les organisations et les lieux.
Dans le domaine étudié, ces entités ne sont pas suffisantes, et ce manque a motivé l’extension du processus de reconnaissance pour en ajouter une à base de règles s’appuyant sur le moteur de règles multi-jetons de \emph{spaCy}.
Un exemple de règle permettant d’identifier les sommes d’argent est présenté dans la figure~\ref{fig:money-extract}.
Une entité de type \texttt{MONEY} est ainsi détectée si un nombre---c’est à dire un jeton dont le \emph{part-of-speech} est \texttt{NUM}---est suivi du symbole monétaire \enquote{\$}, avec potentiellement un jeton d’espacement séparant les deux (de tels jetons sont créés par le \emph{tokenizer} lorsqu’une espace insécable est insérée entre le nombre et son unité).

\begin{figure}[htb]
\centering
\begin{minipage}{.5\textwidth}
\begin{lstlisting}[language=Python]
{"label": "MONEY", "pattern": [
    {"POS": "NUM"},
    {"IS_SPACE": True, "OP": "?"},
    {"TEXT": "$"},
]}
\end{lstlisting}
\end{minipage}
\caption{Règle de reconnaissance de l’entité nommée \texttt{MONEY}.}
\label{fig:money-extract}
\end{figure}

D’autres entités utiles à la tâche d’extraction d’informations des décisions incluent les quantités de personnes, par exemple \enquote{24~personnes}, utilisées pour reconnaître la capacité associée à certains permis~(\texttt{CAPACITY})~; les types de permis attribués par la régie, tels que \enquote{restaurant}, \enquote{bar} ou \enquote{loterie vidéo}~(\texttt{PERMIT\_TYPE})~; les références reconnaissables car précédées de la mention~\enquote{numéro}, par exemple les numéros de permis~(\texttt{REFERENCE})~; ou encore les durées, comme \enquote{3 jours} ou \enquote{2 mois}, utilisées pour reconnaître la durée de sanctions de suspension de permis~(\texttt{DURATION}).

\subsection{Extraction des informations recherchées}

Dans l’ensemble, ces pré-traitements permettent de transformer les données~HTML de chaque décision en une séquence de jetons en conservant le balisage sémantique existant.
Cette séquence de jetons est enrichie d’entités nommées spécifiques au domaine qui facilitent l’étape finale du processus de traitement consistant à extraire les informations recherchées.
Dans cette section sont exposées les heuristiques que nous avons mis en place pour extraire chaque information identifiée précédemment comme étant utile.

\paragraph{Informations directement disponibles}
Certaines données utiles ont déjà bénéficié d’un balisage suffisamment précis dans le document source.
Dans la mesure où les pré-traitements mis en place précédemment permettent de conserver ce balisage, ces informations peuvent donc être extraites directement.
Il s’agit du numéro de chaque décision, de sa nature, de sa date, des informations sur l’établissement concerné (nom, adresse et responsable), du numéro de dossier en lien, et du titulaire du permis concerné.

\paragraph{Date et lieu de l’audience}
Ces deux informations sont regroupées sous la même balise dans les décisions.
Une heuristique simple pour les séparer est d’utiliser le fait que la date est reconnue comme une entité nommée de type \texttt{DATE} et le lieu une entité de type \texttt{LOC}.

\paragraph{Permis concernés}
Les informations sur les permis concernés par une décision sont toutes confondues dans le même balisage dans les documents sources, il est donc nécessaire d’adopter une stratégie plus spécifique pour extraire chaque information indépendamment.
Chaque permis revêt un numéro~(entité nommée~\texttt{REFERENCE}) et un type~(entité nommée~\texttt{PERMIT\_TYPE}).
Les permis de bars et restaurants sont en plus associés à une capacité maximale de personnes~(entité nommée~\texttt{CAPACITY}).
Puisque la plupart des décisions concernent plusieurs permis, la difficulté est de parvenir à associer les bonnes informations au bon permis.
Une règle simple qui est respectée dans une large majorité des décisions~(voir un exemple dans la figure~\ref{fig:permit-extract}) consiste à considérer que toutes les informations précédant un numéro de permis sont relatives au permis concernées.

\begin{figure}[htb]
\centering
\begin{minipage}[t]{.45\textwidth}
\begin{framed}
PERMIS EN VIGUEUR~:

Restaurant pour vendre, 1\textsuperscript{er} étage, capacité 98,\\
No 7996457;

\vspace{1em}
Restaurant pour vendre, 1er étage avant,\\
capacité 21, no 9823832
\end{framed}
\end{minipage}\hspace{2em}
\begin{minipage}[t]{.35\textwidth}
\begin{lstlisting}[language=Python]
Permit(number="7996457",
       type="Restaurant",
       capacity=98)

Permit(number="9823832",
       type="Restaurant",
       capacity=21)
\end{lstlisting}
\end{minipage}
\caption{Extrait de la décision 40-0006650 du 6~mars~2015 (à gauche, voir~\url{http://canlii.ca/t/ggrlk}) et résultat de l’extraction des permis concernés (à droite).}
\label{fig:permit-extract}
\end{figure}

\paragraph{Verdict prononcé}
Situé à la fin de la décision, après l’exposition de l’affaire et du droit concerné, il se compose d’une série d’actions introduites chacune par une expression en majuscules qui est caractéristique du type d’action en question.
Par exemple, pour imposer la suspension temporaire d’un permis, l’action pourrait être \enquote{\textbf{SUSPEND pour une période d’un jour} le permis de bar numéro 000000}.
Dans ce travail, nous nous sommes concentrés sur quatre types d’actions~: la suspension d’un permis, sa révocation, l’imposition d’une amende, ou l’absence d’intervention.
Là encore, le balisage englobe le verdict sans apporter plus de détails.
Pour séparer le verdict en actions, une possibilité est d’utiliser le fait que chaque action commence par un mot en majuscules situé au début d’une ligne.
Pour les actions de suspension de permis, nous considérons que tous les numéros de permis (entités de type~\texttt{REFERENCE}) apparaissant dans l’action sont concernés par une suspension de la durée mentionnée dans l’action (entité de type~\texttt{DURATION}).
Pour la révocation, la même stratégie est adoptée on omettant la notion de durée.
Enfin le montant des sanctions administratives pécuniaires~(amendes) est détectée en prenant les entités nommées de type \texttt{MONEY} qu’elles contiennent.
Un exemple d’extraction de verdict est présenté en figure~\ref{fig:verdict-extract}.

\begin{figure}[htb]
\centering
\begin{minipage}[t]{.45\textwidth}
\begin{framed}
\textbf{POUR CES MOTIFS, LE TRIBUNAL DE LA RÉGIE DES ALCOOLS, DES COURSES ET DES JEUX :}

\vspace{1em}
\textbf{ENTÉRINE} la proposition conjointe, laquelle sera annexée à la présente décision pour en faire partie intégrante;

\vspace{1em}
\textbf{SUSPEND pour une période d’un (1) jour} les permis de bar n\textsuperscript{os} 642306, 642314, 642322, 8760142, 8760175, 9197641, 9313438, 9313446, 9328428, 9833195, 9833203 et 9833211, et ce, à compter de la mise sous scellés des boissons alcooliques;

\vspace{1em}
\textbf{ORDONNE} pendant la période de suspension, la mise sous scellés des boissons alcooliques se trouvant sur les lieux, par un inspecteur de la Régie ou par le corps policier dûment mandaté à cette fin;

\vspace{1em}
\textbf{IMPOSE} à la titulaire, Café Bar Le St-Sulpice inc., une sanction administrative pécuniaire juridictionnelle de 1 000 \$.
\end{framed}
\end{minipage}\hspace{2em}
\begin{minipage}[t]{.45\textwidth}
\begin{lstlisting}[language=Python]
PermitSuspend(reference="642306",
              duration="(1) jour")
PermitSuspend(reference="642314",
              duration="(1) jour")
PermitSuspend(reference="642322",
              duration="(1) jour")
PermitSuspend(reference="8760142",
              duration="(1) jour")
PermitSuspend(reference="8760175",
              duration="(1) jour")
PermitSuspend(reference="9197641",
              duration="(1) jour")
PermitSuspend(reference="9313438",
              duration="(1) jour")
PermitSuspend(reference="9313446",
              duration="(1) jour")
PermitSuspend(reference="9328428",
              duration="(1) jour")
PermitSuspend(reference="9833195",
              duration="(1) jour")
PermitSuspend(reference="9833203",
              duration="(1) jour")
PermitSuspend(reference="9833211",
              duration="(1) jour")
Fine(fine="1 000 $")
\end{lstlisting}
\end{minipage}
\caption{Extrait de la décision 40-0008645 du 4~avril~2019 (à gauche, voir~\url{http://canlii.ca/t/hzvwx}) et résultat de l’extraction de son verdict (à droite).}
\label{fig:verdict-extract}
\end{figure}

\section{Exemple de fonctionnement}

L’implémentation du projet, comprenant notamment le script d’extraction, peut être récupérée depuis le dépôt Git suivant~:
\url{https://gitlab.com/remiceres/nlp-canlii}.
Ce script accepte en entrée un chemin vers le fichier source d’une décision, applique le processus de traitement décrit précédemment et affiche le résultat de l’extraction en sortie.
En l’état, ce format est simplement un affichage brut des structures de données Python, mais il est possible d’adapter le programme pour produire les données dans n’importe quel autre format de données structurées.
Voici un exemple de résultat obtenu en lançant le programme sur le fichier \texttt{2017canlii33342.html}~(voir \url{http://canlii.ca/t/h42dw}).

\vspace{1em}
\begin{lstlisting}
(*\bfseries Numéro de la décision :*) 40-0007867 
(*\bfseries Date de la décision :*) 2017-05-16
(*\bfseries Dossier(s) concerné(s) :*) 40-0238493-002
(*\bfseries Date de l'audience :*) 2017-04-24
(*\bfseries Lieu de l'audience :*) Montréal
(*\bfseries Régisseur(s) :*) SAIFO ELMIR, MARC SAVARD, avocat
(*\bfseries Établissement :*) Establishment(name="Dooly's Saint-Hyacinthe", address='1300, rue Johnson Ouest Saint-Hyacinthe (Québec)\xa0 J2S 7K7', manager='M.\xa0Elvis Joubert')
(*\bfseries Permis concerné(s) :*) Permit(number='9347691', type='Bar', capacity=158), Permit(number='71332', type='loterie vidéo', capacity=None), Permit(number='9436973', type='Bar', capacity=19), Permit(number='9897265', type='Bar', capacity=142), Permit(number='9897273', type='Bar', capacity=77), Permit(number='83246', type='loterie vidéo', capacity=None)
(*\bfseries Détenteur du (des) permis :*) Dooly's Saint-Hyacinthe inc.
(*\bfseries Type de décision :*) Contrôle de l’exploitation
(*\bfseries Actions :*) [PermitSuspend(reference='9347691', duration='(2) jours'), PermitSuspend(reference='9436973', duration='(2) jours'), PermitSuspend(reference='9897265', duration='(2) jours'), PermitSuspend(reference='9897273', duration='(2) jours'), PermitSuspend(reference='71332', duration='(2) jours'), PermitSuspend(reference='83246', duration='(2) jours')]
\end{lstlisting}

\section{Évaluation}

Afin de quantifier les performances du système dans sa tâche d’extraction d’informations depuis des décisions de la régie, nous avons décidé de mener une évaluation de celui-ci par rapport à un critère de \emph{précision}, mesurée en terme du ratio entre le nombre d’informations extraites correctement et le nombre total d’informations extraites, et son \emph{rappel}, mesuré en terme du ratio entre le nombre d’informations extraites et le nombre d’informations existantes.
Faute d’avoir à notre disposition des exemples étiquetés, nous avons réalisé cette évaluation manuellement, en nous basant d’une part sur un échantillon de test de cinquante décisions tirées aléatoirement parmi des décisions du corpus que nous avions utilisées pour construire les règles, et d’autre part sur un échantillon de validation de cinquante décisions tirées aléatoirement parmi des décisions récentes que nous n’avions pas consultées pour élaborer les règles.

Pour chacune des décisions de l’échantillon, et pour chacun des onze types d’informations à extraire, trois indicateurs sont relevés~: si l’information est présente dans le $i$-ème document~HTML d’origine~(variable binaire notée $PH_i$), si l’information est présente dans la sortie de l’extraction automatique de ce document~(variable binaire notée $PS_i$), et s’il y a égalité entre les deux informations~(variable binaire notée $EQ_i$).
Remarquons que $PH_i \neq PS_i \Rightarrow EQ_i = 0$ et $PH_i = 0 \wedge PS_i = 0 \Rightarrow EQ_i = 1$~: le seul cas où la variable $EQ_i$ est discriminante est lorsque l’information est présente dans les deux bords, et dans ce cas elle permet d’encoder si le programme a extrait la bonne information ou non.
Ayant relevé ces variables, la précision et le rappel de l’extraction d’une information sont données par \[
    \mathrm{précision}_i = \frac{\sum_i{PS_i \times EQ_i}}{\sum_i{PS_i}}
    \qquad
    \mathrm{rappel}_i = \frac{\sum_i{PS_i \times EQ_i}}{\sum_i{PH_i}}\ .
\]

Le relevé de ces informations, incluant la liste des décisions échantillonnées, peut être téléchargé depuis le lien suivant~: \url{https://next.cloud.delab.re/s/iiPWg2bwHHm7JqW}, et les indicateurs obtenus suite à l’évaluation sont présentés dans le tableau \ref{fig:results}.

\begin{table}[htb]
\centering
\begin{tabular}{l@{\hskip 2.5em}r@{\hskip 0pt}r@{\hskip 2.5em}r@{\hskip 0pt}r}
\toprule
\multirow{2}{*}{\textbf{Information}}
& \multicolumn{2}{c@{\hskip 2.5em}}{\textbf{Précision~(\%)}}
& \multicolumn{2}{c}{\textbf{Rappel~(\%)}} \\
& Test & Valid. & Test & Valid.  \\
\midrule
\multicolumn{5}{l}{\textsc{Informations directement disponibles}}\\[.3em]
Numéro décision & 100	& 100	& 65	& 100 \\
Date décision   & 100	& 100	& 100	& 100 \\
Numéro dossier  & 100	& 100	& 100	& 100 \\
Régisseurs      & 100	& 94	& 92	& 92 \\
Établissement   & 72	& 81	& 67	& 85 \\
Détenteur       & 86	& 100	& 53	& 14 \\
Type décision   & 100	& 100	& 42	& 100 \\
\midrule
\multicolumn{5}{l}{\textsc{Informations décomposées}}\\[.3em]
Date audience   & 100	& 100	& 51	& 92 \\
Lieu audience   & 100	& 92	& 60	& 86 \\
Permis          & 100   & 92    & 42    & 88 \\
Actions         & 83    & 92    & 30    & 56 \\
\bottomrule
\end{tabular}
\caption{Résultat de l’évaluation de la précision et du rappel du programme pour l’extraction d’informations sur deux échantillons de données de cinquante décisions chacun.}
\label{fig:results}
\end{table}

De façon attendue pour un système d’extraction basé sur des règles construites manuellement, la précision obtenue est élevée, avec des résultats variant entre~72\% et 100\% en test et entre~81\% et 100\% en validation, alors que le rappel est plus faible, descendant à~30\% en test et~14\% en validation.
En effet, avec cette approche, les règles construites sont très spécifiques et relèvent rarement des informations erronées mais s'adaptent mal aux variations de formatage du texte.
Par ailleurs, une certaine disparité dans le rappel peut être constatée entre le groupe des informations directement extraites à partir du balisage existant~(partie supérieure du tableau) et le groupe des informations qui ont nécessité une décomposition~(partie inférieure).
Cette disparité s’explique par le fait que davantage de règles sont appliquées pour décomposer des informations telles que les permis ou le verdict.
Enfin, nous pouvons constater que le rappel est significativement plus élevé en validation qu’en test.
Le format des décisions a en effet beaucoup évolué entre celui de l’année~2000 et les formats utilisés actuellement, et les règles que nous avons construites ont tendance à être mieux adaptées aux formats récents.
De plus, certaines anciennes décisions~(avant~2006) ont tendance à manquer de balisage, voire à être manuscrites, ce qui met en échec nos heuristiques en l’état.
Cette différence de rappel illustre donc que le choix de séparation entre les données de validation et les données de test est rétrospectivement discutable, puisque le jeu de données de validation contient un nombre plus important de décisions récentes.
Une meilleure façon de procéder aurait consisté à mettre de côté un ensemble de décisions uniformément distribuées en amont du travail.

\section{Conclusion}

Nous avons réalisé un outil permettant d’automatiser l’extraction d’informations des décisions de la régie des alcools, des courses et des jeux du Québec~(RACJ) pour faciliter la recherche et la prise de décision basée sur des décisions antérieures de la régie.
Ce programme est principalement basé sur un ensemble de règles conçues manuellement et implémentées en utilisant la bibliothèque de traitement automatique de la langue naturelle \textit{spaCy}.
Il est composé de deux phases~: une première phase de pré-traitements vise à préparer le texte des décisions originales en partant du format~HTML pour ne conserver que les informations utiles, une seconde phase d’extraction se base sur le résultat de cette préparation et un ensemble de règles de manière à extraire chaque information recherchée.
À l’issue d’une évaluation manuelle des performances du programme sur un échantillon du corpus, celui-ci démontre une forte précision sur les informations retrouvées mais un rappel encore trop faible pour pouvoir être utilisable en l’état par les employés de la régie.
Pour autant, nous sommes confiants dans le fait que nos travaux peuvent être utilisés comme une base dont il est possible de partir pour y ajouter de nouvelles règles couvrant les cas manquants.
Il est également envisageable d’y ajouter une approche statistique pour améliorer la prise en charge des anciens fichiers de décision dont le balisage met en échec nos règles.

À travers ce projet, nous avons pu mettre en pratique un certain nombre de notions du cours, en particulier celles liées à la normalisation de texte et à l’extraction d’informations basée sur l’élaboration de règles manuelles, en vue de construire un \emph{pipeline} complet pour réaliser une tâche dans un domaine spécifique.
Il a également constitué une opportunité pour prendre en main la bibliothèque \textit{spaCy} et son moteur d’expressions régulières étendues.
Enfin, nous espérons que nos travaux pourront être utiles dans le futur aux employés de la~RACJ pour leur travail.

\end{document}