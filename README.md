# Extraction d’informations des décisions de la RACJ

Ce programme a pour objectif d’extraire automatiquement des informations sémantiques de décisions rendues par la RACJ.
Il implémente pour ce faire un processus de traitement de la langue naturelle basé sur la bibliothèque _spaCy_.

## Utilisation

Une installation fonctionnelle de Python en version 3.7 ou supérieure est requise.
Avant de lancer le programme, la bibliothèque spaCy doit être installée et le modèle de langue du français téléchargé :

```sh
pip install --user spacy
python -m spacy download fr
```

Le script `extract` permet d’extraire les données d’une décision :

```sh
> ./extract data/2010canlii29031.html

Chargement du modèle Spacy… OK
Traitement de data/2010canlii29031.html

Numéro de la décision : 40-0003607
Date de la décision : 2010-05-12
Dossier(s) concerné(s) : 40-0235754-001
Date de l’audience : 2010-04-26
Lieu de l’audience : Québec
Régisseur(s) : ANDRÉE FORTIN, JEAN PROVENCHER, avocat
Établissement : Establishment(name='Bar 4000 enr.', address='11900, 2e Avenue\xa0\xa0 \xa0\xa0 \xa0\xa0 \xa0\xa0 \xa0\xa0 \xa0\xa0 \xa0  Saint-Georges (Québec) G5Y 1X2', manager='Simon Fortin')
Permis concerné(s) : Permit(number='9119983', type='Bar', capacity=105), Permit(number='9119991', type='Bar', capacity=31), Permit(number='9214578', type='Bar', capacity=28), Permit(number='55087', type='loterie vidéo', capacity=None)
Détenteur du (des) permis : Gestion Henri Quirion inc.          
Type de décision :
Actions : [PermitRevoke(reference='9119991')]
```
