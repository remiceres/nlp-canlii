from .model.decision import Decision
from . import fields


def extract(tags):
    permits = fields.permits.process(tags)

    return Decision(
        case_numbers=fields.case_numbers.process(tags),
        decision_number=fields.decision_number.process(tags),
        hearing_date=fields.hearing_date.process(tags),
        hearing_place=fields.hearing_place.process(tags),
        decision_date=fields.decision_date.process(tags),
        regisseurs=fields.regisseurs.process(tags),
        establishment=fields.establishment.process(tags),
        permits=permits,
        permit_holder=fields.permit_holder.process(tags),
        decision_type=fields.decision_type.process(tags),
        actions=fields.actions.process(tags, permits),
    )
