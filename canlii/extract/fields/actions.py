from ..model import actions
from ...preprocess.model import model
from spacy.matcher import Matcher
from spacy.util import filter_spans

tag_name = 'Decision'

action_matcher = Matcher(model.vocab)
action_matcher.add("ActionMatcher", None, [{
    "IS_SPACE": True,
    "TEXT": {"REGEX": r"\n"},
}, {
    "IS_UPPER": True,
    "OP": "+",
}])


def _permit_in_list(permits, number):
    return any(permit.number == number for permit in permits)


def process(tags, permits):
    result = []

    if tag_name in tags:
        decision = tags[tag_name]
        action_tokens = filter_spans([
            decision[start:end]
            for _, start, end in action_matcher(decision)
        ]) + [decision[len(decision):len(decision)]]

        for left, right in zip(action_tokens[:-1], action_tokens[1:]):
            name = str(decision[left.start:left.end]).strip().lower()
            contents = decision[left.end:right.start]

            if name == 'suspend':
                cur_duration = None

                for ent in contents.ents:
                    if ent.label_ == 'DURATION':
                        cur_duration = ent.text
                    if ent.label_ == 'REFERENCE' \
                            and _permit_in_list(permits, ent.text):
                        result.append(actions.PermitSuspend(
                            reference=ent.text,
                            duration=cur_duration,
                        ))
            elif name == 'révoque':
                for ent in contents.ents:
                    if ent.label_ == 'REFERENCE' \
                            and _permit_in_list(permits, ent.text):
                        result.append(actions.PermitRevoke(
                            reference=ent.text,
                        ))
            elif name in ['impose', 'établit', 'confirme']:
                for ent in contents.ents:
                    if ent.label_ == 'MONEY':
                        result.append(actions.Fine(
                            fine=ent.text
                        ))
            elif name == 'n\'intervient pas':
                result.append(actions.NoAction())

    return result
