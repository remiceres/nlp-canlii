from ..model.establishment import Establishment


def process(tags):
    return Establishment(
        name=str(tags.get('NomEtab', '')).strip(),
        address=str(tags.get('Adresse', '')).strip(),
        manager=str(tags.get('Responsable', '')).strip(),
    )
