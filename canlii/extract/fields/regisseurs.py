def process(tags):
    current_index = 1
    regisseurs = []

    while 'NomRegisseur{}'.format(current_index) in tags:
        name = tags['NomRegisseur{}'.format(current_index)]
        regisseurs.append(str(name).strip())
        current_index += 1

    return regisseurs
