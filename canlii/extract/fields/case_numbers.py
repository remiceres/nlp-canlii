tag_name = 'NoDossier'


def process(tags):
    if tag_name in tags:
        return [ent.text
                for ent in tags[tag_name].ents
                if ent.label_ == 'REFERENCE']
    else:
        return []
