from ..model.permit import Permit
import re

tag_name = 'Permis'
number_regex = re.compile(r'\b[0-9]+\b')


def process(tags):
    permits = []

    if tag_name in tags:
        permit_type = None
        permit_capacity = None

        for ent in tags[tag_name].ents:
            if ent.label_ == 'CAPACITY':
                numbers = number_regex.findall(ent.text)

                if len(numbers) == 1:
                    permit_capacity = int(numbers[0])
            elif ent.label_ == 'PERMIT_TYPE':
                permit_type = ent.text
            elif ent.label_ == 'REFERENCE':
                permits.append(
                    Permit(
                        number=ent.text,
                        type=permit_type,
                        capacity=permit_capacity,
                    )
                )
                permit_type = None
                permit_capacity = None

    return permits
