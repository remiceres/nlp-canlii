from datetime import date


def date_from_str(date_str):
    try:
        return date(*map(int, date_str.split('-')))
    except ValueError:
        return None
