def process(tags):
    if 'Titulaire' in tags:
        return tags['Titulaire']
    elif 'Demandeur' in tags:
        return tags['Demandeur']
    else:
        return None
