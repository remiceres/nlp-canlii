from .util import date_from_str

tag_name = 'DateDecision'


def process(tags):
    if tag_name in tags:
        dates = [entity.text
                 for entity in tags[tag_name].ents
                 if entity.label_ == 'DATE']

        if len(dates) == 1:
            return date_from_str(dates[0])

    return None
