tag_name = 'DateAudition'


def process(tags):
    if tag_name in tags:
        locs = [entity.text
                for entity in tags[tag_name].ents
                if entity.label_ == 'LOC']

        if len(locs) == 1:
            return locs[0]

    return None
