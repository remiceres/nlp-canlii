from dataclasses import dataclass
from datetime import date
from typing import List
from .establishment import Establishment
from .permit import Permit


@dataclass
class Decision:
    decision_number: str  # 40-0003624
    decision_date: date  # YYYY-MM-DD
    case_numbers: List[str]  # 40-2303055-001
    hearing_date: date  # YYYY-MM-DD
    hearing_place: str  # Québec
    regisseurs: List[str]  # ["John Doe", "Jean Dupont"]
    establishment: Establishment  # Restaurant Le Cachot
    permits: List[Permit]
    permit_holder: str  # 9113-8461 Québec Inc
    decision_type: str
    actions: List

    def __str__(self):
        return f"""Numéro de la décision : {self.decision_number}
Date de la décision : {self.decision_date}
Dossier(s) concerné(s) : {', '.join(self.case_numbers)}
Date de l’audience : {self.hearing_date}
Lieu de l’audience : {self.hearing_place}
Régisseur(s) : {', '.join(self.regisseurs)}
Établissement : {self.establishment}
Permis concerné(s) : {', '.join(str(permit) for permit in self.permits)}
Détenteur du (des) permis : {self.permit_holder}
Type de décision : {self.decision_type}
Actions : {self.actions}"""
