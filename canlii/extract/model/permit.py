from dataclasses import dataclass


@dataclass
class Permit:
    number: str  # 7951593
    type: str  # Bar, restaurant, loterie vidéo
    capacity: int
