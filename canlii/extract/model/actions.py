from dataclasses import dataclass


@dataclass
class PermitSuspend:
    reference: str
    duration: str


@dataclass
class PermitRevoke:
    reference: str


@dataclass
class Fine:
    fine: int


@dataclass
class NoAction:
    pass
