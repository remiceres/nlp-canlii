from dataclasses import dataclass


@dataclass
class Establishment:
    name: str
    address: str
    manager: str
