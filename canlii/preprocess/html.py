from html.parser import HTMLParser
from .segment import Segment, SegmentKind
import re

whitespace_sequence = re.compile(r'[\n\r\t ]+')


class _TagParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.in_table_row = False
        self.in_uppercase = None
        self.depth = 0
        self.content = []

    def _add_content(self, content):
        # Ajout à la fin du précédent élément de texte si possible
        if self.content and self.content[-1].kind == SegmentKind.Text:
            self.content[-1].content += content
        else:
            self.content.append(Segment(SegmentKind.Text, content))

    def handle_starttag(self, name, attrs_list):
        self.depth += 1

        for key, value in attrs_list:
            if key == 'style' and 'text-transform:uppercase' in value:
                self.in_uppercase = self.depth

        if name == 'a':
            attrs = dict(attrs_list)

            # Détection des tags de métadonnées
            if 'name' in attrs and 'href' not in attrs \
                    and 'class' not in attrs:
                name = attrs['name']

                if name.startswith('Fin'):
                    self.content.append(Segment(
                        SegmentKind.TagEnd,
                        name[3:]
                    ))
                else:
                    self.content.append(Segment(
                        SegmentKind.TagStart,
                        name
                    ))
        elif name == 'tr':
            self.in_table_row = True
            self._add_content('\n')
        elif name == 'p':
            if not self.in_table_row:
                self._add_content('\n\n')

    def handle_endtag(self, name):
        if self.depth == self.in_uppercase:
            self.in_uppercase = None

        self.depth -= 1

        if name == 'tr':
            self.in_table_row = False

    def handle_data(self, content):
        data = whitespace_sequence.sub(' ', content)

        if self.in_uppercase is not None:
            data = data.upper()

        self._add_content(data)


def read(data):
    parser = _TagParser()
    parser.feed(data)
    return parser.content
