import sys
from .segment import SegmentKind


class _MetaTagParser():

    def __init__(self):
        self.tags = {}

        self.in_tag = False
        self.current_tag_name = ''
        self.current_tag_content = ''

    def starttag(self, tag_name):
        self.in_tag = True
        self.current_tag_name = tag_name
        self.current_tag_content = ''

    def endtag(self):
        self.tags[self.current_tag_name] = self.current_tag_content
        self.in_tag = False

    def extract(self, content):
        for segment in content:

            if segment.kind == SegmentKind.TagStart:
                if self.in_tag:
                    print(
                        'Avertissement : La balise <{1}> a été ouverte avant '
                        'que <{0}> n’ait été fermée. <{0}> sera implicitement '
                        'fermée en ce point.'
                        .format(self.current_tag_name, segment.content),
                        file=sys.stderr
                    )
                    self.endtag()

                self.starttag(segment.content)

            elif segment.kind == SegmentKind.TagEnd:
                if segment.content != self.current_tag_name or not self.in_tag:
                    print(
                        'Avertissement : La balise <{}> a été fermée avant '
                        'd’avoir été ouverte. Cette fermeture sera ignorée.'
                        .format(segment.content),
                        file=sys.stderr
                    )
                else:
                    self.endtag()

            elif self.in_tag:
                self.current_tag_content += segment.content

        return self.tags


def process(content):
    parser = _MetaTagParser()
    return parser.extract(content)
