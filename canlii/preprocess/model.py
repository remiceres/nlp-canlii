import re
from spacy.matcher import Matcher
import spacy
from spacy.tokens import Span, Token
from spacy.pipeline import EntityRuler, Sentencizer
import sys

print('Chargement du modèle Spacy… ', end='', flush=True, file=sys.stderr)
model = spacy.load('fr_core_news_sm', disable=['parser'])
print('OK', file=sys.stderr)

# Coupe les mots contenant des unités monétaires
infixes = model.Defaults.infixes + [r'\$']
infix_regex = spacy.util.compile_infix_regex(infixes)
model.tokenizer.infix_finditer = infix_regex.finditer


class NumberMerger:
    """Fusionne les nombres contenant des espaces en un seul jeton."""

    def __init__(self, model):
        self.matcher = Matcher(model.vocab)
        self.matcher.add("NumberMatcher", None, [{"LIKE_NUM": True}])

    def __call__(self, doc):
        numbers = self.matcher(doc)

        cur_start = None
        cur_end = None

        with doc.retokenize() as retok:
            for _, start, end in numbers:
                if cur_start is None:
                    cur_start = start
                    cur_end = end
                else:
                    if all(tok.is_space for tok in doc[cur_end:start]):
                        cur_end = end
                    else:
                        self._merge(doc, retok, cur_start, cur_end)
                        cur_start = start
                        cur_end = end

            if cur_start is not None:
                self._merge(doc, retok, cur_start, cur_end)

        return doc

    def _merge(self, doc, retok, start, end):
        retok.merge(doc[start:end], attrs={"POS": "NUM"})


number_merger = NumberMerger(model)
model.add_pipe(number_merger, name="number_merger", first=True)

Token.set_extension("is_date", default=False)
Token.set_extension("is_reference", default=False)


class ReferenceMerger:
    """Détecte et fusionne les références alphanumériques et les dates."""

    date_regex = re.compile(r'^\d{4}-\d{1,2}-\d{1,2}$')

    def __init__(self, model):
        self.matcher = Matcher(model.vocab)
        self.matcher.add("ReferenceMatcher", None, [{
            "TEXT": {
                # Suite de chiffres et lettres contenant au moins un chiffre
                "REGEX": r"^[0-9A-Z]*[0-9][0-9A-Z]*$"
            }
        }])

    def __call__(self, doc):
        numbers = self.matcher(doc)

        cur_start = None
        cur_end = None

        with doc.retokenize() as retok:
            for _, start, end in numbers:
                if cur_start is None:
                    cur_start = start
                    cur_end = end
                else:
                    if all(tok.text == '-' for tok in doc[cur_end:start]):
                        cur_end = end
                    else:
                        self._merge(doc, retok, cur_start, cur_end)
                        cur_start = start
                        cur_end = end

            if cur_start is not None:
                self._merge(doc, retok, cur_start, cur_end)

        return doc

    def _merge(self, doc, retok, start, end):
        total_text = ''.join(tok.text for tok in doc[start:end])

        if end - start > 1:
            if self.date_regex.match(total_text):
                retok.merge(doc[start:end], attrs={
                    "POS": "PROPN",
                    "_": {"is_date": True},
                })
            elif len(total_text) >= 5:
                retok.merge(doc[start:end], attrs={
                    "POS": "PROPN",
                    "_": {"is_reference": True},
                })


ref_merger = ReferenceMerger(model)
model.add_pipe(ref_merger, name="reference_merger", after="number_merger")

sentencizer = Sentencizer([".", "!", "?"])
model.add_pipe(sentencizer, name="sentencizer", after="reference_merger")

rule_ner = EntityRuler(model)
rule_ner.add_patterns([
    {
        "label": "CAPACITY",
        "pattern": [
            {"POS": 'NUM'},
            {"IS_SPACE": True, "OP": "?"},
            {"LOWER": "personnes"}
        ]
    },
    {
        "label": "CAPACITY",
        "pattern": [
            {"LOWER": "capacité"},
            {"IS_SPACE": True, "OP": "?"},
            {"POS": 'NUM'}
        ]
    },

    {
        "label": "REFERENCE",
        "pattern": [
            {"POS": "PROPN", "_": {"is_reference": True}},
        ],
    },

    {
        "label": "DATE",
        "pattern": [
            {"POS": "PROPN", "_": {"is_date": True}},
        ],
    },

    {
        "label": "DURATION",
        "pattern": [
            {"TEXT": "(", "OP": "?"},
            {"POS": "NUM"},
            {"TEXT": ")", "OP": "?"},
            {"LEMMA": {"IN": ["jour", "mois", "an"]}},
        ]
    },

    {"label": "PERMIT_TYPE", "pattern": [{"LOWER": "bar"}]},
    {"label": "PERMIT_TYPE", "pattern": [{"LOWER": "restaurant"}]},
    {"label": "PERMIT_TYPE", "pattern": [
        {"LOWER": "loterie"}, {"LOWER": "vidéo"},
    ]},
    {"label": "PERMIT_TYPE", "pattern": [{"LOWER": "palefrenier"}]},

    {"label": "MONEY", "pattern": [
        {"POS": "NUM"},
        {"IS_SPACE": True, "OP": "?"},
        {"TEXT": "$"},
    ]}
])

model.add_pipe(rule_ner, name="rule_ner", after="tagger")


class ReferenceRuler:
    """Détecte les références précédées de “numéro” comme telles."""

    def __init__(self, model):
        self.matcher = Matcher(model.vocab)
        self.matcher.add("ReferencePrefix", None, [
            {"LOWER": {"IN": ["nos", "numéros", "numéro", "no", "n°"]}}
        ])

    def __call__(self, doc):
        matches = self.matcher(doc)

        for (_, _, end) in matches:
            cursor = end

            while cursor < len(doc):
                if doc[cursor].pos_ == 'NUM':
                    entity = Span(doc, cursor, cursor + 1, label="REFERENCE")

                    try:
                        doc.ents += (entity,)
                    except ValueError as err:
                        if 'E103' in err.args[0]:
                            # S’il existe déjà une entité nommée sur le
                            # segment concerné, arrêt de la boucle
                            break
                        else:
                            raise
                elif not (doc[cursor].text == ','
                          or doc[cursor].pos_ in ['SPACE', 'CCONJ']):
                    # Si la référence n’est pas suivie d’un élément conjonctif
                    # ou d’une espace, arrêt de la séquence courante
                    break

                cursor += 1

        return doc


ref_ner = ReferenceRuler(model)
model.add_pipe(ref_ner, name="reference_ner", after="rule_ner")
