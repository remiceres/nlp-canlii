from dataclasses import dataclass
from enum import Enum


class SegmentKind(Enum):
    Text = 0
    TagStart = 1
    TagEnd = 2


@dataclass
class Segment():
    kind: SegmentKind
    content: str
