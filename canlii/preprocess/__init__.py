from . import html
from .model import model
from . import tags


def preprocess(stream):
    # Transforme le HTML en flux de texte et tags
    res_content = html.read(stream.read())

    # Extraction des tags
    res_tags = tags.process(res_content)

    # Prétraitement du contenu des tags
    for key, value in res_tags.items():
        res_tags[key] = model(res_tags[key])

    return res_tags
